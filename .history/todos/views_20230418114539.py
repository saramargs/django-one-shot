from django.shortcuts import render
from todos.models import ToDoList
from todos.forms import ToDoForm


def todo_list_list(request):
    todos = ToDoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = ToDoList.objects.get(id=id)
    context = {"todo_object": todo}
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = ToDoForm(request.POST)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            model_instance = form.save()
            return redirect("detail_url", id=model_instance.id)

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = ModelForm()

    context = {"form": form}

    return render(request, "model_names/create.html", context)
