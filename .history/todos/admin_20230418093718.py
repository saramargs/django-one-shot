from django.contrib import admin


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "recipe",
        "step_number",
        "id"
    )