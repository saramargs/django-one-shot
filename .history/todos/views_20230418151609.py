from django.shortcuts import render, redirect, get_object_or_404
from todos.models import TodoList, TodoItem
from todos.forms import ToDoForm, ToDoItemForm


def todo_list_list(request):
    todos = ToDoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = ToDoList.objects.get(id=id)
    context = {"todo_object": todo}
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = ToDoForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_list")

    else:
        form = ToDoForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    todo = get_object_or_404(ToDoList, id=id)
    if request.method == "POST":
        form = ToDoForm(request.POST, instance=todo)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            form.save()
            return redirect("todo_list_detail", id=todo.id)

    else:
        form = ToDoForm(instance=todo)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    todo = ToDoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        todo = ToDoItemForm(request.POST)
        if todo.is_valid():
            item = todo.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = ToDoItemForm()
        context = {"form": form}
        return render(request, "items/create.html", context)


def todo_item_update(request, id):
    todo = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = ToDoItemForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=todo.id)

    else:
        form = ToDoItemForm(instance=todo)

    context = {"form": form}

    return render(request, "todos/edit.html", context)
