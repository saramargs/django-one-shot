from django.shortcuts import render
from todos.models import ToDoList, ToDoItem


def todo_list(request):
    todolist = ToDoList.objects.all()
    context = {
        "todo_list": ToDoList,
    }
    return render(request, "recipes/list.html", context)