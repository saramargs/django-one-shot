from django.forms import ModelForm
from todos.models import ToDoList, TodoItem


class ToDoForm(ModelForm):
    class Meta:
        model = ToDoList
        fields = ("name",)


class ToDoItemForm(ModelForm):
    class Meta:
        model = ToDoItem
        fields = ("task", "due_date", "is_completed", "list")
