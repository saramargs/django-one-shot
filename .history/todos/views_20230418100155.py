from django.shortcuts import render
from todos.models import ToDoList, ToDoItem


def todo_list(request):
    recipes = ToDoList.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)