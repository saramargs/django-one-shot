from django.shortcuts import render
from todos.models import 


def todo_list(request):
    recipes = ToDoList.objects.all()
    context = {
        "recipe_list": recipes,
    }
    return render(request, "recipes/list.html", context)