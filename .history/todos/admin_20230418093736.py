from django.contrib import admin
from todos.apps import ToDoList


@admin.register(ToDoList)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        "recipe",
        "step_number",
        "id"
    )