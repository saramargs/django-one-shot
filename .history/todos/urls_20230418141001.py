from django.urls import path
from todos.views import (
    todo_list_list,
    show_todo,
    create_todo,
    edit_todo,
    delete_todo,
    create_todo_item
)

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    path("<int:id>/", show_todo, name="todo_list_detail"),
    path("create/", create_todo, name="todo_list_create"),
    path("<int:id>/edit/", edit_todo, name="todo_list_edit"),
    path("<int:id>/delete/", delete_todo, name="todo_list_delete"),
    path("items/create/", todo_item_create, name="todo_item_create"),
]
