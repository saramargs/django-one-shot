from django.shortcuts import render, redirect, get_object_or_404
from todos.models import ToDoList
from todos.forms import ToDoForm, ToDoItemForm


def todo_list_list(request):
    todos = ToDoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = ToDoList.objects.get(id=id)
    context = {"todo_object": todo}
    return render(request, "todos/detail.html", context)


def create_todo(request):
    if request.method == "POST":
        form = ToDoForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("todo_list_list")

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = ToDoForm()

    context = {"form": form}

    return render(request, "todos/create.html", context)


def edit_todo(request, id):
    todo = get_object_or_404(ToDoList, id=id)
    if request.method == "POST":
        form = ToDoForm(request.POST, instance=todo)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            form.save()
            return redirect("todo_list_detail", id=todo.id)

            # To add something to the model, like setting a user,
            # use something like this:
            #
            # model_instance = form.save(commit=False)
            # model_instance.user = request.user
            # model_instance.save()
            # return redirect("detail_url", id=model_instance.id)
    else:
        form = ToDoForm(instance=todo)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def delete_todo(request, id):
    todo = ToDoList.objects.get(id=id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


def create_todo_item(request):
    if request.method == "POST":
        form = ToDoItemForm(request.POST)
        if form.is_valid():
            recipe = form.save(False)
            recipe.author = request.user
            recipe.save()
            return redirect("list_recipes")
    else:
        form = ToDoItemForm()
        context = {
        "form": form
        }
        return render(request, "recipes/create.html", context)
