from django.shortcuts import render, get_object_or_404
from todos.models import ToDoList


def todo_list_list(request):
    todos = ToDoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def show_todo(request, id):
    todo = get_object_or_404(ToDoList, id=id)
    context = {"todo_object": todo}
    return render(request, "todos/detail.html", context)
