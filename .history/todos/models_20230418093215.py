from django.db import models
from datetime import timezone, now


class ToDo(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name
