from django.forms import ModelForm
from todos.models import Recipe

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = ("title", 
                 "picture", 
                 "description")
        
