from django.forms import ModelForm
from todos.models import ToDoList

class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = ("title", 
                 "picture", 
                 "description")
        
