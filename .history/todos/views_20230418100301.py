from django.shortcuts import render
from todos.models import ToDoList, ToDoItem


def todo_list(request):
    todos = ToDoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)