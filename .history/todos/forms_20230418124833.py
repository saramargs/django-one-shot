from django.forms import ModelForm
from todos.models import ToDoList


class ToDoForm(ModelForm):
    class Meta:
        model = ToDoList
        fields = ("name",)
