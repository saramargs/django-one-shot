from django.db import models

# Create your models here.
class ToDo(models.Model):
  name = models.CharField(max_length=100)

  # This tells Django how to convert our model into a string
  # when we print() it, or when the admin displays it.
  def __str__(self):
    return self.name