from django.forms import ModelForm
from todos.models import ToDoList


class TodoForm(ModelForm):
    class Meta:
        model = ToDoList
        fields = ("name")
        
