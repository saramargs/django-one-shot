from django.forms import ModelForm
from todos.models import ToDoList, ToDoItem


class ToDoForm(ModelForm):
    class Meta:
        model = ToDoList
        fields = ("name",)


class ToDoItemForm(ModelForm):
    class Meta:
        model = ToDoItem
        fields = ("name",)
