from django.shortcuts import render


def show_model_name(request):
  model_list = ModelName.objects.all()
  context = {
    "model_list": model_list
  }
  return render(request, "model_names/list.html", context)