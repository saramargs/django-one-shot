from django.shortcuts import render
from todos.models import ToDoList, ToDoItem


def todo_list(request):
    recipes = ToDoList.objects.all()
    context = {
        "todo_list": recipes,
    }
    return render(request, "recipes/list.html", context)