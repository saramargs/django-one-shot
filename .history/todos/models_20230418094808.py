from django.db import models
from django.utils import timezone


class ToDoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.name


class ToDoItem(models.Model):
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(required=False)
    is_completed = models.BooleanField()
